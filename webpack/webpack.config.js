const path = require('path');
const autoprefixer = require('autoprefixer');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const glob = require('glob');
const ManifestRevisionPlugin = require('manifest-revision-webpack-plugin');

const TARGET = process.env.WEBPACK_TARGET || 'DEV';

const PATHS = {
    app: path.join(__dirname, '../web/assets'),
    build: path.join(__dirname, '../web/static/dist')
};

const VENDOR = [
    'react',
    'react-dom',
    'jquery',
];

const basePath = path.resolve(__dirname, '../web/static/');

const common = {
    context: basePath,
    entry: {
        vendor: VENDOR,
        app: PATHS.app
    },
    output: {
        path: PATHS.build,
        publicPath: "/static/dist/",
        filename: "[name].[chunkhash].js",
        chunkFilename: "[id].chunk.js",
        library: ["APP","[name]"],
        libraryTarget: "var"  
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true,
            minChunks: 2
        }),
        new ManifestRevisionPlugin(path.join(basePath, 'manifest.json'), {
            rootAssetPath: '../web/assets',
            ignorePaths: ['/js', '/styles']
        }),        
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: TARGET === 'dev' ? '"development"' : '"production"' },
            '__DEVELOPMENT__': TARGET === 'dev'
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            'Popper': ['popper.js', 'default'],
        }),
        new CleanWebpackPlugin([PATHS.build], {
            root: process.cwd()
        }),
        new ExtractTextPlugin({
            filename: '[name].[chunkhash].min.css',
            allChunks: true
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.optimize\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: true
        })          
    ],
    resolve: {
        extensions: ['.jsx', '.js', '.json', '.scss', '.css'],
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: /node_modules/
            },
            { test: /\.jsx?/, exclude: /node_modules/, loader: "babel-loader" },
            {
                test: /\.css$/,  
                include: /node_modules/,  
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [
                    'css-loader?minimize'
                ]})
            },
            { 
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [
                    'css-loader?minimize',
                    'sass-loader'
                ]})
            },            
            {
                test: /\.jpe?g$|\.gif$|\.png$/,
                loader: 'file-loader?name=/images/[name].[ext]?[hash]'
            },
            {
                test: /\.woff(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/font-woff2'
            },
            {
                test: /\.ttf(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]'
            },
            {
                test: /\.otf(\?.*)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]&mimetype=application/font-otf'
            },
            {
                test: /\.svg(\?.*)?$/,
                loader: 'url-loader?name=/fonts/[name].[ext]&limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.json(\?.*)?$/,
                loader: 'file-loader?name=/files/[name].[ext]'
            }
        ]
    },
};

switch (TARGET) {
    case 'DEV':
        module.exports = merge(require('./webpack.dev'), common);
        break;
    case 'PROD':
        module.exports = merge(require('./webpack.prod'), common);
        break;
    default:
        console.log('Target configuration not found. Valid targets: "dev" or "prod".');
}