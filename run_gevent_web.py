from gevent import monkey
monkey.patch_all()

from psycogreen.gevent import patch_psycopg
patch_psycopg()

import argparse
from gevent.wsgi import WSGIServer

from geventwebsocket.handler import WebSocketHandler
from gevent.pool import Pool

import logging

def main():
    parser = argparse.ArgumentParser(description='Run web flask dev server')
    parser.add_argument('-p', '--port', type=int, default=8000,
                        help='port to run web app')
    parser.add_argument('-L', '--log-level', default='DEBUG', help='log level')                        
    args = parser.parse_args()

    log_levels = {
        'debug': logging.DEBUG,
        'info': logging.INFO,  
        'warning': logging.WARNING,
        'error': logging.ERROR,        
        'critical': logging.CRITICAL
    }
    
    port = args.port
    host = "0.0.0.0"

    from web import db, setup_sockets, create_app

    db.set_threadlocal(True)
    app = create_app()
    setup_sockets(app)

    logging.basicConfig(level=log_levels[args.log_level.lower()])

    #pool_size = 16
    #worker_pool = Pool(pool_size)
    http_server = WSGIServer((host, port), app, 
        handler_class=WebSocketHandler,
    #   pool=worker_pool
    )
    http_server.serve_forever()

if __name__ == '__main__':
    main()