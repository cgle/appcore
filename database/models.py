import bcrypt
import uuid
from datetime import datetime, timedelta
from sqlalchemy import Table, Column, String, ForeignKey, DateTime, Integer, Float, UnicodeText, Boolean, MetaData, UniqueConstraint, inspect
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils.types import TSVectorType
from sqlalchemy_searchable import make_searchable
from sqlalchemy.orm.session import Session
import shortuuid
from slugify import slugify

Model = declarative_base()
metadata = Model.metadata
make_searchable(metadata)

def _unique(session, cls, hashfunc, queryfunc, constructor, arg, kw):
    cache = getattr(session, '_unique_cache', None)
    if cache is None:
        session._unique_cache = cache = {}

    key = (cls, hashfunc(*arg, **kw))
    if key in cache:
        return cache[key]
    else:
        with session.no_autoflush:
            q = session.query(cls)
            obj = queryfunc(q, *arg, **kw).first()            
            if not obj:
                obj = constructor(*arg, **kw)
                session.add(obj)
        cache[key] = obj
        return obj

class ModelMixin(object):

    id = Column(Integer, primary_key=True)

    def update(self, **kwargs):        
        for k, v in kwargs.items():
            if hasattr(self, k) and (v != getattr(self, k)):                
                setattr(self, k, v)

    def to_dict(self):
        raise NotImplementedError
    
class TimeStampMixin(object):

    create_time = Column(DateTime, default=datetime.utcnow)
    update_time = Column(DateTime, onupdate=datetime.utcnow, default=datetime.utcnow)

class UserMixin(object):

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return self.id
        except AttributeError:
            raise NotImplementedError('No id attribute - override get_id')

    def __eq__(self, other):
        if isinstance(other, UserMixin):
            return self.get_id() == other.get_id()
        return NotImplemented

    def __ne__(self, other):
        equal = self.__eq__(other)
        if equal is NotImplemented:
            return NotImplemented
        return not equal

class UniqueMixin(object):

    @classmethod
    def unique_hash(cls, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def unique_filter(cls, query, *arg, **kw):
        raise NotImplementedError()

    @classmethod
    def as_unique(cls, session, *arg, **kw):
        return _unique(
                    session,
                    cls,
                    cls.unique_hash,
                    cls.unique_filter,
                    cls,
                    arg, kw
               )

class User(Model, ModelMixin, TimeStampMixin, UserMixin):

    __tablename__ = 'user'

    email = Column(String(255), unique=True, nullable=False)
    _password = Column(String(255))

    first_name = Column(String(255), default='')
    last_name = Column(String(255), default='')
    is_account_confirmed = Column(Boolean, default=False)    
    account_confirmed_time = Column(DateTime)

    def __hash__(self):
        return hash(self.email)

    @hybrid_property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(12)).decode('utf8')

    def check_password(self, password):
        return bcrypt.checkpw(password.encode('utf8'), self._password.encode('utf8'))

    @hybrid_property
    def is_confirmed(self):
        return self.is_account_confirmed