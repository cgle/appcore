from .user import UserService

def init_defaults(db):
    services = {service.name for service in [
        UserService(db)
    ]}

    return services
