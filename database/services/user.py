from .base import DatabaseService
import database.models as models

class UserService(DatabaseService):

    name = 'user'
    Model = models.User

    def get_by_email(self, email):
        return self.filter_one(email=email)

    def update_by_email(self, email, **kwargs):
        user = self.get_by_email(email)
        if not user:
            raise RuntimeError('User {} not found'.format(email))

        user.update(**kwargs)
        self.db.commit()
        return user

    def delete_by_email(self, email):
        user = self.get_by_email(email)
        if not user:
            raise RuntimeError('User {} not found'.format(email))

        self.db.session.delete(user)
        self.db.commit()