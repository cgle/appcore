from sqlalchemy import and_, or_

class DatabaseService(object):

    name = None
    Model = None

    def __init__(self, db):
        self.db = db

        if hasattr(db, self.name):
            raise AttributeError('service {} exists in db'.format(self.name))

        setattr(db, self.name, self)

    def init_db(self, db):
        if self.db:
            self.db.shutdown()
            self.db = None
        self.db = db

    def query(self):
        return self.db.query(self.Model)

    def add(self, **kwargs):    
        item = self.Model()
        # ALWAYS add to session first
        self.db.session.add(item)
        item.update(**kwargs)
        self.db.commit()
        return item

    def get_first(self):
        return self.db.query(self.Model).first()

    def get_all(self, limit=100):
        return self.db.query(self.Model).limit(limit).all()

    def get_or_create(self, **kwargs):
        items = self.filter_by(**kwargs)
        if items:
            return items[0]
        else:
            return self.add(**kwargs)

    def filter(self, *args, **kwargs):
        limit = kwargs.pop('limit', None)
        filter_rule = and_(*args)
        return self.db.query(self.Model).filter(filter_rule).limit(limit).all()

    def filter_q(self, *args):
        filter_rule = and_(*args)
        return self.db.query(self.Model).filter(filter_rule)

    def filter_q_or(self, *args):
        filter_rule = or_(*args)
        return self.db.query(self.Model).filter(filter_rule)

    def filter_by(self, **kwargs):
        limit = kwargs.pop('limit', None)
        return self.db.query(self.Model).filter_by(**kwargs).limit(limit).all()

    def filter_by_q(self, **kwargs):
        return self.db.query(self.Model).filter_by(**kwargs)


    def filter_one(self, **kwargs):
        items = self.filter_by(**kwargs)
        if not items:
            return None
        return items[0]

    def get_by_short_id(self, short_id):
        return self.filter_one(short_id=short_id)

    def get_by_id(self, id):
        return self.db.query(self.Model).get(id)

    def update_by_id(self, id, **kwargs):
        item = self.db.query(self.Model).get(id)
        if item is None:
            raise RuntimeError('{} {} not found'.format(self.name, id))
        item.update(**kwargs)
        self.db.commit()
        return item

    def delete_by_id(self, id):
        item =self.db.query(self.Model).get(id)
        if item is None:
            raise RuntimeError('{} {} not found'.format(self.name, id))
        self.db.session.delete(item)
        self.db.commit()

    def update_one(self, item, **kwargs):
        item.update(**kwargs)
        self.db.commit()
        return item

    def delete_one(self, item):
        self.db.session.delete(item)
        self.db.commit()

    def get_column_names(self):
        return self.Model.__table__.columns.keys()