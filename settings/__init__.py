import os
import ujson
from collections import Mapping

base_dir = os.path.dirname(os.path.realpath(__file__))

def update(d, u):
    for k, v in u.items():
        if isinstance(v, Mapping):
            r = update(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d

def load(*fnames):
    conf = {}

    mode_affix = 'prod' if os.getenv('SETTINGS_MODE') == 'PROD' else 'dev'
    
    for fname in fnames:
        file_path = priority_load(fname, mode_affix)
        this_conf = ujson.loads(open(file_path, 'r').read())
        update(conf, this_conf)
    return conf

def priority_load(fname, mode_affix):
    file_paths = (
            os.path.join(base_dir, '.'.join((fname, mode_affix, 'config'))),
            os.path.join(base_dir, '.'.join((fname, 'dev', 'config') )),
            os.path.join(base_dir, '.'.join((fname, 'config') )) 
            )
    for fp in file_paths:
        if os.path.isfile(fp):
            return fp
    return None
