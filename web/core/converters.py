from slugify import slugify
from werkzeug.routing import BaseConverter
from flask import current_app as app

class IDSlugConverter(BaseConverter):

    regex = r'-?\d+(?:/[\w\-]*)?'

    def __init__(self, map, attr='name', model=None):        
        self.attr = attr
        self.model = getattr(app.db, model, None)
        super(IDSlugConverter, self).__init__(map)

    def to_python(self, value):
        id, slug = (value.split('/') + [None])[:2]
        return int(id)

    def to_url(self, id):
        if not self.model: return id

        item = self.model.get_by_id(id)
        if not item: return id

        slug = slugify(getattr(item, self.attr))
        return f'{id}/{slug}'.rstrip('/')