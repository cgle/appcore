import bleach
from flask import jsonify, request, url_for, redirect, abort
from urllib.parse import urlparse, urljoin
from itsdangerous import URLSafeTimedSerializer
from flask import current_app as app
import requests

cleaner = bleach.Cleaner(
    tags = [
        'a', 'h1', 'h2', 'h3', 'strong', 'em', 'p', 'ul', 'ol',
        'li', 'br', 'sub', 'sup', 'hr', 'img', 'table', 'thead', 'tbody', 'tr', 'th', 'td',
        'div'
    ],
    attributes = {
        'a': ['href', 'name', 'target', 'title', 'id'], 
        'img': ['src'],
        'div': ['style'],
        'table': ['style']
    },
    strip=True,
    strip_comments=True,
    styles=[]
)

def clean_list(seq, idfun=None): 

    idfun = idfun if idfun is not None else lambda x: x

    seen = {}
    result = []
    for item in seq:
        tmp = None if item in ('', 'None', 'null', 'none') else item

        try:                
            #tmp = self._sanitizer.sanitize(tmp)
            tmp = cleaner.clean(tmp)                
        except:                
            pass

        marker = idfun(tmp)
        if marker in seen: continue
        seen[marker] = 1
        result.append(tmp)

    return result

def serialize_args():
    data = {}
    try:
        keys = list(request.values.keys())

        for k in keys:            
            items = clean_list(request.values.getlist(k))
            data[k] = items
            if items:
                data[k] = items if len(items) > 1 else items[0]

    except (AttributeError, TypeError, ValueError):                   
        pass
    return data    

def api_output(**kwargs):
    code = kwargs.pop('code', 200)
    kwargs['code'] = code
    return jsonify(kwargs), code

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def get_redirect_target():
    for target in request.values.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target
    
    return ''

def redirect_back(endpoint, **values):
    target = request.form['next']
    if not target or not is_safe_url(target):
        target = url_for(endpoint, **values)
    return redirect(target)

def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['ACCOUNT_CONFIRM_SECRET'])
    return serializer.dumps(email, salt=app.config['ACCOUNT_CONFIRM_SALT'])

def check_confirmation_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(app.config['ACCOUNT_CONFIRM_SECRET'])
    try:
        email = serializer.loads(
            token,
            salt=app.config['ACCOUNT_CONFIRM_SALT'],
            max_age=expiration            
        )
    except:
        return False

    return email

def send_mailgun(to, subject, html=None, text=None, cc=None, bcc=None, send_async=True):
    url = app.config["MAILGUN_BASE_URL"] + "/messages"

    return requests.post(
        url,
        auth=('api', app.config['MAILGUN_API_KEY']),
        data={
            'from': '{} Support <{}>'.format(app.config['APP_NAME'], app.config['MAIL_DEFAULT_SENDER']),
            'to': to,
            'subject': subject,
            'text': text,
            'html': html
        }
    )

def verify_recaptcha(response=None, remote_ip=None):
    url = app.config['RECAPTCHA_URL']
    data = {
        "secret": app.config['RECAPTCHA_SITE_SECRET'],
        "response": response or request.form.get('g-recaptcha-response'),
        "remoteip": remote_ip or request.environ.get('REMOTE_ADDR')
    }
    
    output = requests.post(
        url,
        data=data
    )
    
    return output.json()["success"] if output.status_code == 200 else False