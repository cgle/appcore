from functools import wraps
from flask import request, current_app, redirect, url_for, abort
from flask_login import current_user, login_fresh
from flask_login.config import EXEMPT_METHODS
from flask_jwt_extended import jwt_required
from flask_jwt_extended.config import config
from flask_jwt_extended.utils import (verify_token_claims)
from flask_jwt_extended.view_decorators import (_decode_jwt_from_request, 
    verify_token_claims, verify_jwt_in_request, verify_jwt_in_request_optional)
from flask_jwt_extended.exceptions import (UserClaimsVerificationError,
    NoAuthorizationError, InvalidHeaderError)
import htmlmin

try:
    from flask import _app_ctx_stack as ctx_stack
except ImportError:  # pragma: no cover
    from flask import _request_ctx_stack as ctx_stack

from .utils import api_output, verify_recaptcha

def new_jwt_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:      
            ctx_stack.top.jwt = {'identity': current_user.id}
            return fn(*args, **kwargs)

        verify_jwt_in_request()
        return fn(*args, **kwargs)

    return wrapper

def new_jwt_optional(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            ctx_stack.top.jwt = {'identity': current_user.id}
            return fn(*args, **kwargs)

        verify_jwt_in_request_optional()        
        return fn(*args, **kwargs)

    return wrapper

def new_jwt_required_with_confirm(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            if not current_user.is_confirmed:
                raise NoAuthorizationError('User is not confirmed')            
            ctx_stack.top.jwt = {'identity': current_user.id}
            return fn(*args, **kwargs)

        verify_jwt_in_request()
        return fn(*args, **kwargs)

    return wrapper

def new_jwt_optional_with_confirm(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if current_user.is_authenticated:
            if not current_user.is_confirmed:
                raise NoAuthorizationError('User is not confirmed')
            ctx_stack.top.jwt = {'identity': current_user.id}
            return fn(*args, **kwargs)

        try:
            jwt_data = _decode_jwt_from_request(request_type='access')
            ctx_stack.top.jwt = jwt_data
            if not verify_token_claims(jwt_data[config.user_claims]):
                raise UserClaimsVerificationError('User claims verification failed')
            _load_user(jwt_data[config.identity_claim])
        except (NoAuthorizationError, InvalidHeaderError):
            pass
        return fn(*args, **kwargs)
    return wrapper

def new_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if request.method in EXEMPT_METHODS:
            return func(*args, **kwargs)
        elif current_app.login_manager._login_disabled:
            return func(*args, **kwargs)
        elif not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()
        elif not current_user.is_confirmed:
            return redirect(url_for("auth.unconfirmed"))
        return func(*args, **kwargs)
    return decorated_view

def new_fresh_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if request.method in EXEMPT_METHODS:
            return func(*args, **kwargs)
        elif current_app.login_manager._login_disabled:
            return func(*args, **kwargs)
        elif not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()
        elif not login_fresh():
            return current_app.login_manager.needs_refresh()
        elif not current_user.is_confirmed:
            return redirect(url_for("auth.unconfirmed"))
        return func(*args, **kwargs)
    return decorated_view    

def uglify(route_function):
    @wraps(route_function)
    def wrapped(*args, **kwargs):
        yielded_html = route_function(*args, **kwargs)
        minified_html = htmlmin.minify(yielded_html)
        return minified_html
    return wrapped

def recaptcha_verified(func):
    @wraps(func)
    # By pass recaptcha
    def decorated_view(*args, **kwargs):
        if current_app.config['RECAPTCHA_DISABLED']:
            return func(*args, **kwargs)
        if not verify_recaptcha():
            return api_output(error='Verifying Recaptcha Error', code=500)
        return func(*args, **kwargs)
    return decorated_view