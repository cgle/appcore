from flask import request, abort, jsonify, views, render_template, url_for, session
from flask_login import login_required, current_user, login_user, logout_user
from flask_jwt_extended import create_access_token, create_refresh_token, set_access_cookies, set_refresh_cookies, unset_jwt_cookies, jwt_required, jwt_refresh_token_required, get_jwt_identity
from rauth import OAuth2Service
from werkzeug import secure_filename
from datetime import datetime 

from web import db, old_login_required
from web.core import utils, decorators
from . import bp

@bp.route('/auth.is_logged_in')
def is_logged_in():
    resp = jsonify({'is_logged_in': current_user.is_authenticated})
    return resp, 200

@bp.route('/auth.request_token', methods=['POST'])
def auth_request_token():
    args = utils.serialize_args()
    user = db.user.get_by_email(args['email'])

    if user is None:
        return jsonify({'error': 'Not authenticated', 'code': 401}), 401

    if not user.check_password(args['password']):
        return jsonify({'error': 'Invalid password', 'code': 401}), 401

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    return jsonify({'msg': 'Successfully requested token',
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'code': 200}), 200

@bp.route('/auth.refresh_token', methods=['POST'])
@jwt_refresh_token_required
def auth_refresh_token():
    user_id = get_jwt_identity()
    access_token = create_access_token(identity=user_id)

    resp = jsonify({'refresh_token': True,
                    'access_token': access_token,
                    'code': 200})

    set_access_cookies(resp, access_token)

    return resp, 200

@bp.route('/auth.remove_token_cookies', methods=['POST'])
@jwt_required
def auth_remove_token_cookies():
    resp = jsonify({'msg': 'Token cookies removed','remove_token_cookies': True, 'code': 200})
    unset_jwt_cookies(resp)
    return resp, 200

@bp.route('/auth.login', methods=['POST'])
def auth_login():
    if current_user.is_authenticated:
        return jsonify({'error': 'Already logged in', 'code': 500}), 500
    
    ip_client = request.remote_addr
    args = utils.serialize_args()
    user = db.user.get_by_email(args['email'])
   
    if user is None:
        return jsonify({'error': 'Not authenticated', 'code': 401}), 401

    if not user.check_password(args['password']):
        return jsonify({'error': 'Invalid password', 'code': 401}), 401

    login_user(user)

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    resp = jsonify({
        'msg': 'Successfully logged in',
        'access_token': access_token,
        'refresh_token': refresh_token,
        'code': 200})

    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)    
    
    return resp, 200

@bp.route('/auth.register', methods=['POST'])
def auth_register():
    if current_user.is_authenticated:
        return jsonify({'error': 'User already logged in', 'code': 403}), 403

    args = utils.serialize_args()        
    user = db.user.get_by_email(args['email'])

    if user is not None:
        return jsonify({'error': 'User already exists', 'code': 403}), 403
        
    user = db.user.add(**args)

    login_user(user, True)

    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    resp = jsonify({'msg': 'Successfully registered',
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    'code': 200})

    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)
    
    return resp, 200

@bp.route('/auth.logout', methods=['GET', 'POST'])
@old_login_required
def logout():    
    logout_user()
    resp = jsonify({'msg': 'Logged out successfully', 'code': '200'})
    unset_jwt_cookies(resp)
    return resp