from flask import Blueprint
from web.core import utils

bp = Blueprint('api', __name__, url_prefix='/api/v1')

@bp.route('/')
def base():
    return utils.api_output(text='API OK', code=200)

from .hello import *
from .auth import *