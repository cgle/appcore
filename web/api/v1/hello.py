from flask import jsonify
from flask_jwt_extended import jwt_required
from . import bp
from web.core import utils

@bp.route('/hello', methods=['GET','POST','PUT','PATCH','DELETE'])
def hello():    
    return utils.api_output(msg='hello world')

@bp.route('/hello/protected', methods=['GET','POST','PUT','PATCH','DELETE'])
@jwt_required
def hello_protected():
    return utils.api_output(msg='hello world')
