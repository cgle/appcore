from flask import render_template, Blueprint, Response, request, redirect, make_response
from flask import current_app as app
import babel
from web.core import utils

bp = Blueprint('index', __name__)

@bp.route('/')
def default():
    return render_template('index.html')

# By default use English
@bp.route('/change-lang')
def change_lang():
    redirect_url = redirect(utils.get_redirect_target())
    response = make_response(redirect_url)

    newlang = request.args.get('lang_code', 'en')
    if newlang in app.config['SUPPORTED_LANGUAGES']:
        response.set_cookie('lang_code', value=newlang)
    return response

