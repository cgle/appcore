from flask import render_template, Blueprint
from flask_login import login_required

bp = Blueprint('hello', __name__)

@bp.route('/hello')
def default():
    return render_template('hello.html')

@bp.route('/hello/protected')
@login_required
def protected():
    return render_template('hello.html')
