from flask import render_template, Blueprint, make_response, url_for, redirect
from flask_login import login_required, logout_user, current_user
from flask_jwt_extended import unset_jwt_cookies
from web.forms.auth import LoginForm, RegisterForm
bp = Blueprint('auth', __name__)

@bp.route('/login')
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index.default'))    
    form = LoginForm()
    return render_template('auth/login.html', form=form)

@bp.route('/register')
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index.default'))        
    form = RegisterForm()
    return render_template('auth/register.html', form=form)

@bp.route('/logout')
@login_required
def logout():
    logout_user()
    resp = make_response(redirect(url_for('index.default')))
    unset_jwt_cookies(resp)
    return resp
