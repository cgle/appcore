import os
import glob
import flask
from flask import Flask, _app_ctx_stack, g, render_template, abort, request, url_for, redirect
from werkzeug.exceptions import (
    Forbidden,
    InternalServerError,
    HTTPException,
    NotFound
)
from flask_login import LoginManager
from flask_webpack import Webpack
import flask_login
import flask_jwt_extended
from flask_wtf.csrf import CSRFProtect
from functools import partial
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import event
from redis import StrictRedis
from flask_babel import Babel
from flask_humanize import Humanize
from celery import Celery
from flask_session import Session
import sys
import ujson
import locale
import re

from database import Database
from database.models import metadata
import settings
from .core import converters, utils, decorators

basedir = os.path.dirname(os.path.realpath(__file__))
location = lambda x: os.path.join(basedir, x)
parentdir = os.path.abspath(location('..'))

def load_config():
    mode_affix = 'dev'
    current_mode = os.getenv('SETTINGS_MODE', 'DEV')
    if current_mode == 'PROD':
        mode_affix = 'prod'
    
    conf = settings.load(
        'web.base', 
        'web'
    )

    conf.update(DATABASE=settings.load('db'))
    conf.update(CELERY=settings.load('celery'))

    return conf

app_config = load_config()
sess = Session()
csrf = CSRFProtect()

flask_jwt_extended.jwt_required = decorators.new_jwt_required
flask_jwt_extended.jwt_optional = decorators.new_jwt_optional

old_login_required = flask_login.login_required
old_fresh_login_required = flask_login.fresh_login_required

if not app_config['DEBUG']:
    flask_jwt_extended.jwt_required = decorators.new_jwt_required_with_confirm
    flask_jwt_extended.jwt_optional = decorators.new_jwt_optional_with_confirm
    flask_login.login_required = decorators.new_login_required
    flask_login.fresh_login_required = decorators.new_fresh_login_required

    flask.render_template = decorators.uglify(flask.render_template)

db = Database(app_config['DATABASE']['database_uri'], metadata,
              engine_options=app_config['DATABASE']['engine_options'],
              session_options=app_config['DATABASE']['session_options'],
              scopefunc=_app_ctx_stack.__ident_func__)
r = StrictRedis(
    host=app_config['REDIS_HOST'],
    port=app_config['REDIS_PORT'],
    db=app_config['REDIS_DB']
)
login_manager = LoginManager()
jwt = flask_jwt_extended.JWTManager()
webpack = Webpack()
babel = Babel()
humanize = Humanize()
CLR = Celery(**app_config['CELERY'])

def create_app(conf=app_config, setup=True, **kwargs):
    app = Flask(__name__,
                static_url_path='/static',
                static_folder=location('static'),
                template_folder=location('templates'))

    app.config.update(conf)
    app.config.update(kwargs)
    app.url_map.strict_slashes = False
    app.basedir = basedir
    app.parentdir = parentdir

    if setup:
        with app.app_context():
            setup_session(app)
            setup_jinja(app)
            setup_babel(app)
            setup_extensions(app)
            setup_api(app)
            setup_url_converters(app)
            setup_views(app)
            setup_celery(app)
            setup_humanize(app)
            setup_cli(app)

    return app

def setup_url_converters(app):
    app.url_map.converters['id_slug'] = converters.IDSlugConverter

def setup_session(app):
    # USE REDIS
    app.config.update({
        'SESSION_TYPE': 'redis',
        'SESSION_REDIS': r
    })

    sess.init_app(app)


def setup_jinja(app):

    #################
    # JINJA GLOBALS #
    #################

    app.jinja_env.policies['json.dumps_function'] = ujson.dumps

    ######################
    # TEMPLATE EXTENSION #
    ######################
    app.jinja_env.add_extension('jinja2.ext.do')
    #app.jinja_env.add_extension('jinja2.ext.i18n')

    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    ######################
    # TEMPLATE FILTERS   #
    ######################    
    from .core import template_filters    

def setup_extensions(app):

    @app.teardown_appcontext
    def shutdown_session(response_or_exc=None):
        if db.session:        
            db.session.remove()
        return response_or_exc

    csrf.init_app(app)

    jwt.init_app(app)    
    jwt._set_error_handler_callbacks(app)
    
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'

    @login_manager.user_loader
    def load_user(id):
        return db.user.get_by_id(id)

    webpack.init_app(app)

    db.init_app(app)

def setup_babel(app):
    babel.init_app(app)

    @babel.localeselector
    def get_locale():        
        if 'lang_code' in request.cookies:
            lang = request.cookies['lang_code']
            if lang in app.config['SUPPORTED_LANGUAGES']:
                return lang
        return 'en'

    @babel.timezoneselector
    def get_timezone():
        user = g.get('user', None)
        if user is not None:
            return user.timezone

def setup_humanize(app):
    humanize.init_app(app)
    
    @humanize.localeselector
    def get_locale():        
        if 'lang_code' in request.cookies:
            lang = request.cookies['lang_code']
            if lang in app.config['SUPPORTED_LANGUAGES']:
                return lang
        return 'en'

def setup_api(app):

    from .api.v1 import bp as api_v1
    
    @api_v1.errorhandler(SQLAlchemyError)
    def api_db_error(e):
        return utils.api_output(error=str(e), code=500)

    @api_v1.errorhandler(Exception)
    def api_generic(e):
        raise(InternalServerError)

    @api_v1.errorhandler(NotFound)
    def api_404(e):
        return utils.api_output(error='Not found', code=404)

    @api_v1.errorhandler(Forbidden)
    def api_403(e):
        return utils.api_output(error='Forbidden', code=403)

    @api_v1.errorhandler(InternalServerError)
    def api_500(e):
        return utils.api_output(error='Internal server error', code=500)

    @api_v1.errorhandler(HTTPException)
    def api_http_exception(e):
        return utils.api_output(error=e.description, code=e.code)

    def api_error(e):
        return utils.api_output(error=e.description, code=e.code)

    @app.errorhandler(404)
    @app.errorhandler(405)
    def _handle_api_error(e):
        if request.path.startswith('/api/'):
            return api_error(e)
        else:
            return e

    app.register_blueprint(api_v1)

def setup_views(app):
    from web.views import (hello, index, auth)

    app.register_blueprint(hello.bp)
    app.register_blueprint(index.bp)
    app.register_blueprint(auth.bp)

def setup_sockets(app):

    from flask_sockets import Sockets

    sockets = Sockets(app)

def setup_celery(app):

    TaskBase = CLR.Task

    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    CLR.Task = ContextTask

def setup_js_variables(app):
    pass

def setup_cli(app):
    pass